clear all;
close all;

% Set default figure position for comparable display
set(groot, 'defaultFigureUnits','pixels');
MP = get(0, 'MonitorPositions');
% Use position of monitor 2 and set default figure to 1490x873 pixel
set(groot, 'defaultFigurePosition', [MP(2,1:2), 1490 873]);
% Save default figure position - necessary due to fig2svg
figPosDF	= get(groot, 'defaultFigurePosition');

addpath('../commonPicSource',...
        './fig2svg/src');

% Load image data 'penguin'
load('penguinViews.mat');

% Font (size) commands:
% ('{', '}' and '$' need to replaced later on since fig2svg does not export them)
%\slb...\elb	for \footnotesize{$...$}          	(here used for labels)
%\nms...\nme	for \normalsize{...}                (here used for captions)

% Turn off warnings since chosen labels do not have a valid interpreter syntax
warning('off')

fig=figure('Color', 'white');
xLabel_list	= {'y', 'x', 'y'}; 
yLabel_list	= {'x', 'z', 'z'};
for i=1:3
    ax(i)=subplot(2,2,i+1);
    axis equal tight;
    box(ax(i),'on');
    xlabel(['\lbs',xLabel_list{i},'\lbe']);
    ylabel(['\lbs',yLabel_list{i},'\lbe']);
    colormap(penguin.colormap);
    hold on;
end

pause(0.1);
warning('on')

% Plot figures
imagesc(ax(1), penguin.y, penguin.x, penguin.img_top);
imagesc(ax(2), penguin.x, penguin.z, penguin.img_side);
imagesc(ax(3), penguin.y, penguin.z, penguin.img_front);
set(ax(1),'YDir','reverse');

% set figure size and default figure size (due to fig2svg)
set(groot, 'defaultFigurePosition',figPosDF-[0 0 9e2 1.5e2]);
set(fig, 'Position',figPosDF-[0 0 9e2 1.5e2]);

% set axes size and position
for i=1:3
    set(ax(i),'Position',ax(i).Position+[0 9e-2 0 0]);
end
set(ax(2),'Position', [0.1300 0.15 0.3355    0.4108])
set(ax(3),'Position', [0.5703 0.15 0.3355    0.4108])

% put subcaptions
warning('off')
title_list={'a','b','c'};
for i=1:3
    ti(i)	= title(ax(i),['\nms(',title_list{i},')\nme']);
    set(ti(i),'Units', 'normalized');
    set(ti(i),'VerticalAlignment','top');
    set(ti(i),'FontWeight','normal');
    % move 'title' to the bottom of the plot:
    set(ti(i),'Position',ti(i).Position-[0 1 0])
    % move all 'titles' the same distance from the bottom of the plot:
    set(ti(i),'Position',ti(i).Position-[0 3e-1 0]*ax(1).PlotBoxAspectRatio(2)/ax(i).PlotBoxAspectRatio(2));
end
pause(0.1);
warning('on')

% Create an invisible auxiliary axis (because fig2svg for some reason likes
% to manipulate the caption of the topmost axis, this avoids this)
axA=axes('Color','none');
set(axA,'Position',ax(1).Position);
set(axA,'PlotBoxAspectRatio',ax(1).PlotBoxAspectRatio);
set(axA,'Visible','off');

% Produce SVG. This is a bottleneck, since I still haven't found a perfect
% method to export SVG's from Matlab that preserves glyphs.
% fig2svg works, but the code is buggy, I woudn't recommend it
% (fig2svg also complains that the chosen labels do not have a valid interpreter syntax)
fname='trickySubfig_singlePDFTEX';
warning('off')
fig2svg([fname,'.svg'],fig);
pause(0.1);
warning('on')
set(groot, 'defaultFigurePosition', figPosDF);

% Generate a bash-script bash_<fname>; executing the bash-script results in a PDF and a PDF_TEX file
% the bash-script performs the following operations:
fileID = fopen(['bash_',fname],'w');
fprintf(fileID,'#!/bin/bash\n\n');
% ...export PDF-LaTeX from SVG using inkscape (generated with 'pdf2svg')
fprintf(fileID,'inkscape -D -z --file=%s.svg --export-pdf=%s.pdf --export-latex\n',fname,fname);
% ... set default font size in figure to \\scriptsize
fprintf(fileID,'sed -i -e ''s/\\\\begingroup%%/\\\\begingroup\\\\scriptsize%%/g'' %s.pdf_tex\n',fname);
% ... replace '\lbs...\lbe' by '\footnotesize{$...$}'
fprintf(fileID,'sed -i -e ''s/\\\\lbs/\\\\footnotesize{$/g''            %s.pdf_tex\n',fname);
fprintf(fileID,'sed -i -e ''s/\\\\lbe/$}/g''                            %s.pdf_tex\n',fname);
% ... replace '\nms...\nme' by '\normalsize{...}'
fprintf(fileID,'sed -i -e ''s/\\\\nms/\\\\normalsize{/g''               %s.pdf_tex\n',fname);
fprintf(fileID,'sed -i -e ''s/\\\\nme/}/g''                             %s.pdf_tex\n',fname);
fclose(fileID);



% pos = cell2mat(arrayfun(@(i) ax(i).Position, 1:3, 'UniformOutput', false)');
% set(ax(1),'XLim',penguin.y([1 end]));
% set(ax(1),'YLim',penguin.x([1 end]));
% set(ax(2),'XLim',penguin.x([1 end]));
% set(ax(2),'YLim',penguin.z([1 end]));
% set(ax(3),'XLim',penguin.y([1 end]));
% set(ax(3),'YLim',penguin.z([1 end]));
