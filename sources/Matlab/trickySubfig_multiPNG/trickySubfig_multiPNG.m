clear all;
close all;
clc;

% Set default figure position for comparable display
set(groot, 'defaultFigureUnits','pixels');
MP = get(0, 'MonitorPositions');
% Use position of monitor 2 and set default figure to 1490x873 pixel
set(groot, 'defaultFigurePosition', [MP(2,1:2), 1490 873]);

addpath('../commonPicSource');

% Load image data 'penguin'
load('penguinViews.mat');

xLabel_list	= {'y', 'x', 'y'}; 
yLabel_list	= {'x', 'z', 'z'};
for i=1:3    
    fig(i)=figure('Color', 'white');
    ax(i)=axes;
    axis equal tight;
    box(ax(i),'on');
    xlabel(xLabel_list{i});
    ylabel(yLabel_list{i});    
    colormap(penguin.colormap);
    hold on;
end

% Plot figures
imagesc(ax(1), penguin.y, penguin.x, penguin.img_top);
imagesc(ax(2), penguin.x, penguin.z, penguin.img_side);
imagesc(ax(3), penguin.y, penguin.z, penguin.img_front);
set(ax(1),'YDir','reverse');

% set figure size
for i=1:3
    % reduce figure size
    set(fig(i),'Position', fig(i).Position./[1 1 2 2]);
    % set aspect ratio of figure according to aspect ratio of axes
    % ax(i).PlotBoxAspectRatio(2)/ax(i).PlotBoxAspectRatio(1) = fig(i).Position(4)/fig(i).Position(3)
    set(fig(i),'Position', [fig(i).Position(1:2), fig(i).Position(4)*ax(i).PlotBoxAspectRatio(1)/ax(i).PlotBoxAspectRatio(2), fig(i).Position(4)])
end

% Draw auxiliary axes above the figure (for png output)
for i=1:3
    axA(i)=axes(fig(i),'Color','none');
    box(axA(i),'on');
    set(axA(i),'xtick',[]);
    set(axA(i),'xticklabel',[]);
    set(axA(i),'ytick',[]);
    set(axA(i),'yticklabel',[]);
    set(axA(i),'Position',ax(i).Position);
    set(axA(i),'PlotBoxAspectRatio',ax(i).PlotBoxAspectRatio);
end

% Export PNGs
fname	= 'trickySubfig_multiPNG';
for i=1:3
    saveas(fig(i),[fname,'_',num2str(i),'.png'])
end



