\section{Lowercase `L' in math mode}

Lower case `L' can be akward in math  mode, consider using `\lstinline|\ell|'. Compare:
\begin{LTXexample}[width=0.55\textwidth]
\begin{align*}
l	&= 1		\\
\ell	&= 1
\end{align*}
\end{LTXexample}

% ------------------------------------------------------------

\section{Fonts in math mode}

The `\lstinline|amsfonts|' package provides a fraktur, calligraphic and blackboard bold font, the latter two for uppercase characters only:
\begin{LTXexample}[width=0.55\textwidth]
$\mathfrak{\ABCPrintU}$	\\
$\mathfrak{\ABCPrintL}$	\\
$\mathcal{\ABCPrintU}$	\\
$\mathbb{\ABCPrintU}$
\end{LTXexample}
(`\lstinline|\ABCPrintU|', `\lstinline|\ABCPrintL|': custom commands in `definitionsTutorial.tex')

Additional math alphabeths can be defined, e.g.\ define the following in the preamble
\begin{lstlisting}
\DeclareFontFamily{OT1}{pzc}{}
\DeclareFontShape{OT1}{pzc}{m}{it}{<-> s * [1.150] pzcmi7t}{}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}
\end{lstlisting}
as an option for a calligraphic math font with lower-case characters:
\begin{LTXexample}[width=0.55\textwidth]
$\mathpzc{\ABCPrintU}$	\\
$\mathpzc{\ABCPrintL}$
\end{LTXexample}

Similarly, define the following in the preamble
\begin{lstlisting}
\DeclareMathAlphabet{\mathbfcal}{OMS}{cmsy}{b}{n}
\DeclareMathAlphabet{\mathbfit}{OT1}{cmr}{bx}{it}
\end{lstlisting}
for combinations of bold and caligraphic style or bold and italic style
\begin{LTXexample}[width=0.62\textwidth]
$\mathbfcal{\ABCPrintU}$	\\
$\mathbfit{\ABCPrintU}$	\\
$\mathbfit{\ABCPrintL}$
\end{LTXexample}

\begin{penguinOpinion}
Math fonts should not be used because one ran out of variables, but rather because the font carries a certain meaning, e.g.\\
`Let $\mathcal{G}$ be the group ($G$,+), where $G$ is a set $G=\{g_1,g_2,\ldots,g_N\}$ ...'
\end{penguinOpinion}


% ------------------------------------------------------------

\section{Exponents, superscripts and subscripts}

The typesetting of exponents and superscripts in \LaTeX\ is the same, which might cause confusion, consider using `\lstinline|{}|' if paranteheses are unsuitable. Compare:
\begin{LTXexample}[width=0.18\textwidth]
\begin{align*}
	\left(a_m\right)^n =& a_m^n	\\ 
		% n might be mistaken for a superscript on the RHS
	\left(a_m\right)^n =& {a_m}^n	\\
		% less ambiguous meaning of n
\end{align*}
\end{LTXexample}

\begin{penguinOpinion}
Double superscripts or subscripts (e.g.\ `\lstinline|$a_m_n$|') cause an error. This can be avoided with `\lstinline|{}|', but it's awfull typesetting, don't do it.
\begin{LTXexample}[width=0.18\textwidth]
\begin{align*}
	\left(a^m\right)^n =& {a^m}^n	\\	% unhelpful RHS
	a^{\left(m^n\right)} =& a^{m^n}	\\	% doubtful RHS
\end{align*}
\end{LTXexample}
Likewise, one might be tempted to define superscripted/subscripted variables with curly brackets if one uses a notation like ${a_m}^n$ anyways - again, don't do it. You save debugging but risk bad typesetting.
E.g., when defining a variable `\lstinline|$\egVarA$|' in the preamble as
\begin{lstlisting}
\newcommand{\egVarA}{{a^m}}
\end{lstlisting}
the following might go unnoticed since it does not give an error
\begin{LTXexample}[width=0.18\textwidth]
\begin{equation*}
	\egVarA^n
\end{equation*}
\end{LTXexample}
\end{penguinOpinion}

% ------------------------------------------------------------

\section{Text in math mode}

Text can be included in math mode:
\begin{LTXexample}[width=0.45\textwidth]
\begin{equation*}
	\left(a^m\right)^n = a^{m\, n}
		\qquad\text{for }a,m,n>0
\end{equation*}
\end{LTXexample}

\begin{penguinOpinion}
Text in math mode can also be useful to clearly name subscripts. Confusion can arise when e.g.\ labeling the pressure $p$ between individual penguins with a subscript `$p$', compare the following:
\begin{LTXexample}[width=0.18\textwidth]
$p_p$		\\% confusing: variable with identical subscript
$p_\text{p}$	  % less confusing: a text-mode subscript
\end{LTXexample}
\end{penguinOpinion}

% ------------------------------------------------------------

\section{One labelled line in math mode, many unlabelled ones}

To label only a single line in a multiline equation in `\lstinline|\align|', labels can be supressed with `\lstinline|\notag|':
\begin{LTXexample}[width=0.39\textwidth]
\begin{align}
	A &= a + b + c + d + e + f	\notag\\
	  &\quad  + g + h + i + j + k	\notag\\
	  &\quad  + l + m + n + o + p
	  	\label{eq:FewLabels1}
\end{align}
\end{LTXexample}
More conveniently, define the following in the preamble
\begin{lstlisting}
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}
\end{lstlisting}
to label selected lines in an `\lstinline|\align*|' environment:
\begin{LTXexample}[width=0.39\textwidth]
\begin{align*}
	A &= a + b + c + d + e + f	\\
	  &\quad  + g + h + i + j + k	\\
	  &\quad  + l + m + n + o + p
	  	\numberthis\label{eq:FewLabels2}
\end{align*}
\end{LTXexample}
Or label all lines collectively by making use of nested math environments (see \S\ref{toDo})
\begin{LTXexample}[width=0.39\textwidth]
\begin{equation}
	\begin{aligned}
		A &= a + b + c + d + e + f	\\
		  &\quad  + g + h + i + j + k	\\
		  &\quad  + l + m + n + o + p
	\end{aligned}
	\label{eq:FewLabels3}
\end{equation}
\end{LTXexample}
The position of the label \eqref{eq:FewLabels3} in the above equation can also be set to the first (`\lstinline|\begin{aligned}[t]|') or last equation line  (`\lstinline|\begin{aligned}[b]|').


%mathfonts
% all math environments
% multiplication (\, \cdot)
%\qquad, \quad

