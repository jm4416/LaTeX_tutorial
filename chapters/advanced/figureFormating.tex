\section{Forcing figure positions}

To enforce figure positions, the position option `\lstinline|\begin{figure}[<position>]|' can be emphatsized with `\lstinline|!|'.

\begin{figure}[t!]
	\centering
	\settowidth{\imagewidth}{\includegraphics{penguin.png}}
	\includegraphics[width=0.2\imagewidth]{penguin.png}
	\caption{The penguin at the top of the page.}
	\label{fig:Penguin_FloatPos_Top}
\end{figure}	

\begin{figure}[h!]
	\centering
	\settowidth{\imagewidth}{\includegraphics{penguin.png}}
	\includegraphics[width=0.2\imagewidth]{penguin.png}
	\caption{The penguin wherever.}
	\label{fig:Penguin_FloatPos_Here}
\end{figure}	

Fig.~\ref{fig:Penguin_FloatPos_Top}, fig.~\ref{fig:Penguin_FloatPos_Here} and fig.~\ref{fig:Penguin_FloatPos_Bot} use the options `\lstinline|t!|', `\lstinline|h!|' and  `\lstinline|b!|' respectively. (note: fig.~\ref{fig:Penguin_FloatPos_Top} would not be placed above the section title with the option `\lstinline|h|').

\begin{figure}[b!]
	\centering
	\settowidth{\imagewidth}{\includegraphics{penguin.png}}
	\includegraphics[width=0.2\imagewidth]{penguin.png}
	\caption{The penguin at the bottom of the page.}
	\label{fig:Penguin_FloatPos_Bot}
\end{figure}	

Another useful command is `\lstinline|\FloatBarrier|' of the `\lstinline|placeins|' package , it prevents a figure or table from crossing the marked position
\newpage

\subsection*{This is a demo subsection}\label{sec:demo_FloatBarrier}

\FloatBarrier

\begin{figure}[t]
	\centering
	\settowidth{\imagewidth}{\includegraphics{penguin.png}}
	\includegraphics[width=0.2\imagewidth]{penguin.png}
	\caption{The penguin at the top of the other page, due to `\lstinline|\\FloatBarrier|'.
	}
	\label{fig:Penguin_FloatBarrier}
\end{figure}	

Fig.~\ref{fig:Penguin_FloatBarrier} would usually be at the top of page~\pageref{sec:demo_FloatBarrier}, since it uses the option `\lstinline|t|'. Because `\lstinline|\FloatBarrier|' prevents this, it is at the to of page~\pageref{fig:Penguin_FloatBarrier} instead:\footnote{
Why are there two back slashes in `\lstinline$\\caption\{...`\\lstinline|\\\\FloatBarrier|'\}$'?
The `\lstinline|\\lstinline|' package is unfortunately fragile. Since only robust commands can be used in moving arguments (such as `\lstinline|\\caption|'), usual `\lstinline|\\lstinline|' syntax throws an error if used in moving arguments. A workarround is to use \LaTeX\ text syntax for `\lstinline|\\|', `\lstinline|\$'|, `\lstinline|\{'|, `\lstinline|\%|' etc. (see \ref{sec:Workflow_movingArg})
}
\begin{lstlisting}
\subsection*{This is a demo subsection}\label{sec:demo_FloatBarrier}

\FloatBarrier

\begin{figure}[t]
	\centering
	\settowidth{\imagewidth}{\includegraphics{penguin.png}}
	\includegraphics[width=0.2\imagewidth]{penguin.png}
	\caption{The penguin at the top of the page, due to `\lstinline|\\FloatBarrier|'.}
	\label{fig:Penguin_FloatBarrier}
\end{figure}	
\end{lstlisting}

If the `\lstinline|placeins|' package is loaded with the option `\lstinline|section|' (as in this document), a `\lstinline|\\FloatBarrier|' is set at every section:
\begin{lstlisting}
\usepackage[section]{placeins}
\end{lstlisting}

% ------------------------------------------------------------

\section{Setting a default path to graphics}\label{sec:graphicspath}

\subsection*{Default path for `\lstinline|\\graphicspath|'}

The command `\lstinline|\graphicspath|' allows one to set a default path for the command `\lstinline|\includegraphics|', e.g. for the present chapter we set: (see `\lstinline|main.tex|')
\begin{lstlisting}
\graphicspath{{./figures/basics/}}
\end{lstlisting}
(Note the double curly bracket! They allow defining multiple default paths:\\ `\lstinline|\graphicspath{{<path1>},{<path2>},...}|').

Thus, the following two examples produce the same output:
\begin{LTXexample}[width=0.3\textwidth,preset=\let\label\orilabel]
% Giving the figure path explicitly:
\begin{figure}[h]
	\centering
	% graphics path:
	\includegraphics[width=0.5\textwidth]{
		./figures/advanced/penguin.png}
	\caption{The penguin:
		expicit figure path.}
	\label{fig:ExplicitFigPath}
\end{figure}	
\vspace*{\baselineskip}

% Using a default path set by \graphicspath
\begin{figure}[h]
	\centering
	% graphics path:
	\includegraphics[width=0.5\textwidth]{
		penguin.png}
	\caption{The penguin:
		using `\lstinline|\\graphicspath|'.}
	\label{fig:graphicsPath}
\end{figure}	
\end{LTXexample}

Using `\lstinline|\graphicspath|' is convenient when including graphic files, but becomes crucial when including graphics as tex-files (see \S\ref{sec:FigAdv_figAsTeX}). The tex-file of such a graphic generated with Inkscape contains `\lstinline|\includegraphics|' commands, e.g.\ \\`\lstinline|LaTeX_tutorial/figures/advanced/penguinFlowchart.pdf_tex|' contains the line:
\begin{lstlisting}
\includegraphics[width=\unitlength,page=1]{penguinFlowchart.pdf}
\end{lstlisting}
Unless the graphic `\lstinline|penguinFlowchart.pdf|' is positioned in the same directory as `\lstinline|main.tex|', the graphic can not be found without setting the default path for `\lstinline|\graphicspath|' to \\`\lstinline|LaTeX_tutorial/figures/advanced/|'.

\subsection*{Default path for `\lstinline|\\input|'}

Setting a default path for the command `\lstinline|\input|' is useful when including graphics as tex-files, though not crucial. For the present chapter we set: (see `\lstinline|main.tex|')
\begin{lstlisting}
\makeatletter
\providecommand*{\input@path}{}
\g@addto@macro\input@path{{./figures/advanced/}}
\makeatother	
\end{lstlisting}
which sets the default path for `\lstinline|\input|' to `\lstinline|LaTeX_tutorial/figures/advanced/|'. The above command lines are the equivalent of `\lstinline|\graphicspath|' for `\lstinline|\input|'.


% ------------------------------------------------------------

\section{Reading in figure sizes}\label{sec:FigAdv_readFigDim}

To include two pictures and preserve their relative size to each other, use `\lstinline|\settowidth|' (`\lstinline|\settoheight|') to read in the figure dimensions and scale relative ro them. For the example below, allocate the following length variable in the preamble:
\begin{lstlisting}
\newlength{\imagewidth}
\end{lstlisting}

\begin{LTXexample}[pos=t,preset=\let\label\orilabel]
\begin{figure}
	\centering
	\settowidth{\imagewidth}{\includegraphics{penguin.png}}
	\includegraphics[width=0.4\imagewidth]{penguin.png}
	\caption{The penguin...}
\end{figure}	
%
\begin{figure}
	\centering
	\settowidth{\imagewidth}{\includegraphics{egg.png}}
	\includegraphics[width=0.4\imagewidth]{egg.png}
	\caption{...and its egg in propper proportion.}
\end{figure}	
\end{LTXexample}

% ------------------------------------------------------------

\section{Subfigures}

Create subfigures with the `\lstinline|subfigure|' environment of the `\lstinline|subcaptions|' package:
\begin{LTXexample}[pos=t,preset=\let\label\orilabel]
\begin{figure}
	\centering
	\begin{subfigure}[t][][t]{0.2\textwidth}
		\centering
	    \includegraphics[width=\textwidth]{penguin.png}
	    \caption{put optional info here}
	    \label{fig:penguin_subfig_VG}
	\end{subfigure}
	\hspace{2 cm}
	\begin{subfigure}[t][][t]{0.2\textwidth}
		\centering
	    \includegraphics[width=\textwidth]{penguin3D.png}
	    \caption{}
	    \label{fig:penguin_subfig_LP}
	\end{subfigure}
	%
	\caption{The penguin in all its glory. As \textbf{(a)} a smooth graphic and \textbf{(b)} a low-poly representation.}
	\label{fig:penguin_subfig}
\end{figure}

\vspace*{\baselineskip}
Fig.~\ref{fig:penguin_subfig} contains subfigures fig.~\ref{fig:penguin_subfig_VG} and fig.~\ref{fig:penguin_subfig_LP}.
\end{LTXexample}

% ------------------------------------------------------------

\section{Figures with \LaTeX\ text formatting}\label{sec:FigAdv_figAsTeX}

Figures can be included with \LaTeX\ text formating (i.e.\ as tex-files), provided the figure is saved in a form that preserves text glyphs, e.g.\ as pdf- or svg-file. Note: text in vector graphics is not always saved as glyhs but sometimes converted to paths, this especially happens when converting from pdf to svg. Inkscape's internal pdf import avoids this.

For a figure that is included as a tex-file, all labels/text within the figures are actual text formated by \LaTeX\ and lines (to some extend) are actual vector graphics. Pixel graphics are included as pdf-files.

\begin{itemize}
	\item advantages:
	\begin{itemize}
		\item text formatting of main text and text in figures is consistent (particularly interessting for math mode and special characters)
		\item text in figures does not scale with the figure size but uses its assigned font size (always remains readable)
		\item commands and definitions can be used within the figure and are updated with each compilation e.g.\ the `\lstinline|\today|' command in fig.~\ref{fig:PDFTEXfigure_flowchart} (cf.\ fig.~\ref{fig:PDFTEXfigure_flowchart_origScreenShot})
	\end{itemize}
	\item disadvantages:
	\begin{itemize}
		\item text placement can be unintuitive when creating a vector file
		\item if the output size of the pdf- and tex-file is based on the exported object size (exported text placement can be unintuitive when creating a vector file
		\item compilation errors can originate from within a tex-file figure 
	\end{itemize}
\end{itemize}

\begin{figure}[b!]
	\centering
	\includegraphics[width=0.5\textwidth]{InkscapeExportDialog.png} 
	\caption{Inkscape dialog for exporting tex- and pdf-files from an svg.}
	\label{fig:InkscapeExportDialog}
\end{figure}

Such a tex-file can be generated from a svg-file (or imported pdf) with Inkscape, either via the GUI with `File'$\rightarrow$`Save a Copy...'$\rightarrow$select pdf format$\rightarrow$`Omit text in PDF and create LaTeX file' (see fig.~\ref{fig:InkscapeExportDialog}) or via the terminal command `\lstinline|inkscape -D -z --file=<in>.svg --export-pdf=<out>.pdf --export-latex|'.
All examples of tex-file figures in this document were generated with Inkscape's pdf-tex export.

All examples of tex-file figures in this document are included by setting a default for the commands `\lstinline|\includegraphics|' and `\lstinline|\input|' as shown in \S\ref{sec:graphicspath}.

\subsection*{Tex-file figures from native vector graphics}

Generating a figure as a tex-file is most intuitive if the figure is already created as a vector graphic, e.g.\ when creating a flow chart. Example fig.~\ref{fig:PDFTEXfigure_flowchart} demonstartes text formating (compare font size of `egg' in fig.~\ref{fig:PDFTEXfigure_flowchart} and fig.~\ref{fig:PDFTEXfigure_flowchart_origScreenShot}) and usage of commands (`\lstinline|\today|').

\begin{LTXexample}[pos=t,preset=\let\label\orilabel]
\begin{figure}
	\centering
	\def\svgwidth{0.5\textwidth}
	\input{penguinFlowchart.pdf_tex}
	\caption{Very abridged penguin evolution. The figure is a tex-file where images are included as pdf-files.}
	\label{fig:PDFTEXfigure_flowchart}
\end{figure}
\end{LTXexample}

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.5\textwidth]{penguinFlowchart_screenshot.png} 
	\caption{Screenshot of the original svg-graphic of fig.~\ref{fig:PDFTEXfigure_flowchart}.}
	\label{fig:PDFTEXfigure_flowchart_origScreenShot}
\end{figure}

Fig.~\ref{fig:PDFTEXfigure_flowchart} was generated from `\lstinline|penguinFlowchart.svg|' with Inkscape, see \\`\lstinline|\LaTeX_tutorial/sources/Inkscape/|'.


\FloatBarrier

How can we use different fonts/font sizes/font styles in a tex-file figure? Fig.~\ref{fig:penguinSize} shows an example where different font sizes and styles are used (as well as a custom command `\lstinline|\Hpeng|', see `\lstinline|definitions.tex|'). 

Note how the different scaling of fig.~\ref{fig:penguinSize_small} and fig.~\ref{fig:penguinSize_big} does not affect the font size.

\begin{LTXexample}[pos=t,preset=\let\label\orilabel\vspace*{0.5 cm}]
\begin{figure}
	\centering
	\hspace*{2cm}
	\begin{subfigure}[b][][b]{0.48\textwidth}
		\centering
		\def\svgwidth{0.8\textwidth}	    
		\input{penguinSize_A.pdf_tex}
	    \caption{}
	    \label{fig:penguinSize_small}
	\end{subfigure}
	\hspace*{- 2cm}
	\begin{subfigure}[b][][b]{0.48\textwidth}
		\centering
		\def\svgwidth{1.2\textwidth}	
		\input{penguinSize_A.pdf_tex}
	    \caption{}
	    \label{fig:penguinSize_big}
	\end{subfigure}
	%
	\caption{How tall is Prof.\ penguin? Fig.~\ref{fig:penguinSize_small} and fig.~\ref{fig:penguinSize_big} show the same tex-file figure at different scalings.}
		\label{fig:penguinSize}
\end{figure}
\end{LTXexample}

The font styles/font sizes in fig.~\ref{fig:penguinSize} were realised as follows:
\begin{itemize}
\item \textbf{italic text:} set the text in the original svg-file in italics, see fig.~\ref{fig:penguinSize_A_origScreenShot} (equivalently possible for bold text)
\item \textbf{font sizes of `How tall is Prof.\ penguin?' and `$\mathbf{\Hpeng}$':} use \LaTeX\ commands (`\lstinline|\normalsize|',`\lstinline|\footnotesize|') in the original svg-file, see fig.~\ref{fig:penguinSize_A_origScreenShot}
\item \textbf{font size of ruler labels:} set a default font size in the figure (`\lstinline|\scriptsize|') by editing the corresponding tex-file `\lstinline|penguinSize_A.pdf_tex|':\\ put in `\lstinline|\scriptsize|' right after the start of the group:
\begin{lstlisting}
\begingroup\scriptsize
\end{lstlisting}
\end{itemize}
Generally, editing of the tex-file should be automated! E.g. the source directory contains a simple bash-script `\lstinline|bash_Svg2PdfTeX|'. Executing this script on the file `\lstinline|penguinSize_B.svg|' via the terminal input
\begin{lstlisting}
. bash_Svg2PdfTeX penguinSize_B
\end{lstlisting}
would generate an equivalent to fig.~\ref{fig:penguinSize}. Compare the both svg-files in fig.~\ref{fig:penguinSize_origScreenShot}.

Fig.~\ref{fig:penguinSize_A_origScreenShot} and \ref{fig:penguinSize_B_origScreenShot} were generated from `\lstinline|fig:penguinSize_A_origScreenShot|' and `\lstinline|fig:penguinSize_A_origScreenShot|', respectively, see \\`\lstinline|\LaTeX_tutorial/sources/Inkscape/|'.

\begin{figure}[b!]
	\centering
	\begin{subfigure}[b][][b]{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{penguinSize_A_screenshot.png} 
	    \caption{}
	    \label{fig:penguinSize_A_origScreenShot}
	\end{subfigure}
	\begin{subfigure}[b][][b]{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{penguinSize_B_screenshot.png} 
	    \caption{}
	    \label{fig:penguinSize_B_origScreenShot}
	\end{subfigure}
	%
	\caption{Screenshot of \textbf{(a)} the original svg-graphic of fig.~\ref{fig:penguinSize} and \textbf{(b)} an svg-graphic that would preduce an output equivalent to fig.~\ref{fig:penguinSize}. To obtain fig.~\ref{fig:penguinSize} from the svg-files, \textbf{(a)} the correspondig tex-file needs to be edited or \textbf{(b)} '\lstinline|bash\_Svg2PdfTeX|' executed (see text).}
		\label{fig:penguinSize_origScreenShot}
\end{figure}

\FloatBarrier

\subsection*{Tex-file figures from non-vector graphics}

How about cases where we do not create the graphic as svg-file? If there exists an option to export to svg (or pdf) while preserving text glyphs and their alignemnt - no problem!

\begin{figure}[t!]
	\centering
	\begin{subfigure}[b][][b]{0.32\textwidth}
		\centering
		\def\svgwidth{0.75\textwidth}
		\input{PDFTEXgraphic_A.pdf_tex}
	    \caption{}
	    \label{fig:PDFTEXgraphic_Matlab_VersA}
	\end{subfigure}
	\begin{subfigure}[b][][b]{0.32\textwidth}
		\centering
		\def\svgwidth{0.75\textwidth}
		\input{PDFTEXgraphic_B.pdf_tex}
	    \caption{}
	    \label{fig:PDFTEXgraphic_Matlab_VersB}
	\end{subfigure}
	\begin{subfigure}[b][][b]{0.32\textwidth}
		\centering
		\def\svgwidth{0.75\textwidth}
		\input{PDFTEXgraphic_C.pdf_tex}
	    \caption{}
	    \label{fig:PDFTEXgraphic_Python}
	\end{subfigure}
	%
	\caption{A penguin outline plotted with \textbf{(a)} Matlab via svg-export with `\lstinline|fig2svg|' (Version A), \textbf{(b)} Matlab via Matlab's pdf-export (Version B) and \textbf{(c)} Python.}
	\label{fig:PDFTEXgraphic}
\end{figure}

\textbf{Matlab (not recommended)}: Unfortunately, I do not know of a perfect way to obtain tex-file figures with Matlab. It is possible though:
\begin{itemize}
\item Version A: Export svg-files with `\lstinline|fig2svg|'; automated export to pdf- and tex-files\\
\textbf{Problem:} `\lstinline|fig2svg|' is buggy and required workarrounds
\item Version B: Export pdf-files with internal Matlab export; automated inport to Inkscape and export to pdf- and tex-files\\
\textbf{Problem:} Matlab's pdf export does not preserve text alignemnt: all text is left aligned (instead of centred for labels)
\end{itemize}
Fig.~\ref{fig:PDFTEXgraphic_Matlab_VersA} and fig.~\ref{fig:PDFTEXgraphic_Matlab_VersB} show the outcomes of version A and B respectively.
See \\`\lstinline|\LaTeX_tutorial/sources/Matlab/PDFTEXgraphic_A/|' and\\ `\lstinline|\LaTeX_tutorial/sources/Matlab/PDFTEXgraphic_B/|' for the Matlab source code.

\textbf{Python (recommended)}: Creating tex-file figures with Python is straightforward with the svg-file export of the `\lstinline|Matplotlib|' library. Fig.~\ref{fig:PDFTEXgraphic_Python} shows an example of a plot created this way. See `\lstinline|\LaTeX_tutorial/sources/Python/PDFTEXgraphic_C/|' for the Python source code.

% ------------------------------------------------------------

\section{Subfigures with tricky alignment}

\subsection*{Option 1}
Solve alignment problem by including a graphic that contains subfigures and (fake) subcaptions (not pretty):
\begin{LTXexample}[pos=t,preset=\let\label\orilabel]
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{trickySubfig_singlePNG.png} 
	\caption{Views of a penguin from different angles: \textbf{(a)} top view, \textbf{(b)} side view, \textbf{(c)} front view. The entire figure is a single png-file.}
	\label{fig:trickySubfig_singlePNG}
\end{figure}

\vspace*{\baselineskip}
Fig.~\ref{fig:trickySubfig_singlePNG} can be referenced, but references to subfigures fig.~\ref{fig:trickySubfig_singlePNG}a, \ref{fig:trickySubfig_singlePNG}b and \ref{fig:trickySubfig_singlePNG}c are workarrounds (though not terrible).
\end{LTXexample}

Fig.~\ref{fig:trickySubfig_singlePNG} was generated with Matlab, see \\`\lstinline|\LaTeX_tutorial/sources/Matlab/trickySubfig_singlePNG/|' for the Matlab source code.

\subsection*{Option 2}
Solve alignment problem using the `\lstinline|subcaptions|' package and an individual graphic per subfigure. Alignment can be to some extend simplified by reading in figure dimensions with `\lstinline|\settowidth|'/`\lstinline|\settoheight|' (see \S\ref{sec:FigAdv_readFigDim}). For the example below, allocate the following length variables in the preamble:
\begin{lstlisting}
\newlength{\reflength}
\newlength{\imagewidth}
\end{lstlisting}
\begin{LTXexample}[pos=t,preset=\let\label\orilabel\vspace*{1 cm}]
\begin{figure}
	\flushright
	\hfill
	%
	\settowidth{\imagewidth}{\includegraphics{trickySubfig_multiPNG_1.png}}
	\settoheight{\imageheight}{\includegraphics{trickySubfig_multiPNG_1.png}}
	\begin{subfigure}[][0.31\imageheight][b]{0.31\imagewidth}
		\centering
	    \includegraphics[width=\textwidth]{trickySubfig_multiPNG_1.png}
	    \caption{}
	    \label{fig:trickySubfig_multiPNG_top}
	\end{subfigure}
	\hspace*{0.05\imagewidth}
	\hspace*{0.15\textwidth}
	%
	\\[1 cm]	
	\hfill
	\settowidth{\imagewidth}{\includegraphics{trickySubfig_multiPNG_2.png}}
	\settoheight{\imageheight}{\includegraphics{trickySubfig_multiPNG_2.png}}
	\begin{subfigure}[][0.45\imageheight][b]{0.45\imagewidth}
		\centering
	    \includegraphics[width=\textwidth]{trickySubfig_multiPNG_2.png}
	    \caption{}
	    \label{fig:trickySubfig_multiPNG_side}
	\end{subfigure}
	%
	\settowidth{\imagewidth}{\includegraphics{trickySubfig_multiPNG_3.png}}
	\settoheight{\imageheight}{\includegraphics{trickySubfig_multiPNG_3.png}}
	\begin{subfigure}[][0.45\imageheight][b]{0.45\imagewidth}
		\centering
	    \includegraphics[width=\textwidth]{trickySubfig_multiPNG_3.png}
	    \caption{}
	    \label{fig:trickySubfig_multiPNG_front}
	\end{subfigure}
	\hspace*{0.15\textwidth}
	%
	\caption{Views of a penguin from different angles: \textbf{(a)} top view, \textbf{(b)} side view, \textbf{(c)} front view. Each subfigure is a individual png-file.}
	\label{fig:trickySubfig_multiPNG}
\end{figure}

\vspace*{\baselineskip}
Fig.~\ref{fig:trickySubfig_multiPNG} can be referenced, as well as the subfigures fig.~\ref{fig:trickySubfig_multiPNG_top},  \ref{fig:trickySubfig_multiPNG_side} and \ref{fig:trickySubfig_multiPNG_front}.
\end{LTXexample}

Fig.~\ref{fig:trickySubfig_multiPNG_top},  \ref{fig:trickySubfig_multiPNG_side} and \ref{fig:trickySubfig_multiPNG_front} were generated with Matlab, see \\`\lstinline|\LaTeX_tutorial/sources/Matlab/trickySubfig_multiPNG/|' for the Matlab source code.

\subsection*{Option 3}
Solve alignment problem by including tex-file that contains subfigures and (fake) subcaptions (see \ref{sec:FigAdv_figAsTeX}).
\begin{LTXexample}[pos=t,preset=\let\label\orilabel]
\begin{figure}[t!]
	\centering
	\def\svgwidth{0.5\textwidth}
	\input{trickySubfig_singlePDFTEX.pdf_tex}
	\caption{Views of a penguin from different angles: \textbf{(a)} top view, \textbf{(b)} side view, \textbf{(c)} front view. The entire figure is a tex-file, where images are included as pdf-files.}
	\label{fig:trickySubfig_singlePDFTEX}
\end{figure}

\vspace*{\baselineskip}
Fig.~\ref{fig:trickySubfig_singlePDFTEX} can be referenced, but references to subfigures fig.~\ref{fig:trickySubfig_singlePDFTEX}a, \ref{fig:trickySubfig_singlePDFTEX}b and \ref{fig:trickySubfig_singlePDFTEX}c are workarrounds (though not terrible).
\end{LTXexample}

Fig.~\ref{fig:trickySubfig_singlePDFTEX} was generated with Matlab, see \\`\lstinline|\LaTeX_tutorial/sources/Matlab/trickySubfig_singlePDFTEX/|' for the Matlab source code.


