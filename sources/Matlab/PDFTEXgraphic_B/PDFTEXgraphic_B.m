clear all;
close all;

% Set default figure position for comparable display
set(groot, 'defaultFigureUnits','pixels');
MP = get(0, 'MonitorPositions');
% Use position of monitor 2 and set default figure to 1490x873 pixel
set(groot, 'defaultFigurePosition', [MP(2,1:2), 1490 873]);

addpath('../commonPicSource');

% Load image data 'penguin'
load('penguinOutline.mat');

% Turn off warnings since chosen labels do not have a valid interpreter syntax
 warning('off')

fig=figure('Color', 'white');
ax=axes;
axis equal tight;
box(ax,'on');
xlabel(['\footnotesize{$x$}']);
ylabel(['\footnotesize{$y$}']);
hold on;

pause(0.1);
warning('on')

% Plot figures
plot(ax, penguin.x, penguin.y, 'k-','linewidth',2);

% set figure size
pos	= fig.Position;
set(fig, 'Position',[pos(1:2) 231 424]);

% Produce PDF. Unfortunately the pdf export does not preserve text alignemnt
% (left aligned instead of centred) 
fname='PDFTEXgraphic_B';
saveas(fig,[fname,'_tmp.pdf'])

% Generate a bash-script bash_<fname>; executing the bash-script results in a PDF and a PDF_TEX file
% the bash-script performs the following operations:
fileID = fopen(['bash_',fname],'w');
fprintf(fileID,'#!/bin/bash\n\n');
% ...export PDF-LaTeX from SVG using inkscape (generated with 'pdf2svg')
fprintf(fileID,'inkscape -z --file=%s_tmp.pdf -D --export-pdf=%s.pdf --export-latex\n',fname,fname);
% ... set default font size in figure to \\scriptsize
fprintf(fileID,'sed -i -e ''s/\\\\begingroup%%/\\\\begingroup\\\\scriptsize%%/g'' %s.pdf_tex\n',fname);
fclose(fileID);




