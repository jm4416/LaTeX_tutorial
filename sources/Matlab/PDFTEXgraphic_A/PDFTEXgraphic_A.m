clear all;
close all;

% Set default figure position for comparable display
set(groot, 'defaultFigureUnits','pixels');
MP = get(0, 'MonitorPositions');
% Use position of monitor 2 and set default figure to 1490x873 pixel
set(groot, 'defaultFigurePosition', [MP(2,1:2), 1490 873]);
% Save default figure position - necessary due to fig2svg
figPosDF	= get(groot, 'defaultFigurePosition');


addpath('../commonPicSource',...
        './fig2svg/src');

% Load image data 'penguin'
load('penguinOutline.mat');

% Font (size) commands:
% ('{', '}' and '$' need to replaced later on since fig2svg does not export them)
%\slb...\elb	for \footnotesize{$...$}          	(here used for labels)
%\nms...\nme	for \normalsize{...}                (here used for captions)

% Turn off warnings since chosen labels do not have a valid interpreter syntax
warning('off')

fig=figure('Color', 'white');
ax=axes;
axis equal tight;
box(ax,'on');
xlabel(['\lbsx\lbe']);
ylabel(['\lbsz\lbe']);
hold on;

pause(0.1);
warning('on')

% Plot figures
plot(ax, penguin.x, penguin.y, 'k-','linewidth',2);

% set figure size and default figure size (due to fig2svg)
set(groot, 'defaultFigurePosition',[figPosDF(1:2) 231 424]);
set(fig, 'Position',[figPosDF(1:2) 231 424]);

% Produce SVG. This is a bottleneck, since I still haven't found a perfect
% method to export SVG's from Matlab that preserves glyphs.
% fig2svg works, but the code is buggy, I woudn't recommend it
% (fig2svg also complains that the chosen labels do not have a valid interpreter syntax)
fname='PDFTEXgraphic_A';
warning('off')
fig2svg([fname,'.svg'],fig);
pause(0.1);
warning('on')
set(groot, 'defaultFigurePosition', figPosDF);

% Generate a bash-script bash_<fname>; executing the bash-script results in a PDF and a PDF_TEX file
% the bash-script performs the following operations:
fileID = fopen(['bash_',fname],'w');
fprintf(fileID,'#!/bin/bash\n\n');
% ...export PDF-LaTeX from SVG using inkscape (generated with 'pdf2svg')
fprintf(fileID,'inkscape -D -z --file=%s.svg --export-pdf=%s.pdf --export-latex\n',fname,fname);
% ... set default font size in figure to \\scriptsize
fprintf(fileID,'sed -i -e ''s/\\\\begingroup%%/\\\\begingroup\\\\scriptsize%%/g'' %s.pdf_tex\n',fname);
% ... replace '\lbs...\lbe' by '\footnotesize{$...$}'
fprintf(fileID,'sed -i -e ''s/\\\\lbs/\\\\footnotesize{$/g''            %s.pdf_tex\n',fname);
fprintf(fileID,'sed -i -e ''s/\\\\lbe/$}/g''                            %s.pdf_tex\n',fname);
% ... replace '\nms...\nme' by '\normalsize{...}'
fprintf(fileID,'sed -i -e ''s/\\\\nms/\\\\normalsize{/g''               %s.pdf_tex\n',fname);
fprintf(fileID,'sed -i -e ''s/\\\\nme/}/g''                             %s.pdf_tex\n',fname);
fclose(fileID);
