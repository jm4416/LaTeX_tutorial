clear all;
close all;
clc;

% Set default figure position for comparable display
set(groot, 'defaultFigureUnits','pixels');
MP = get(0, 'MonitorPositions');
% Use position of monitor 2 and set default figure to 1490x873 pixel
set(groot, 'defaultFigurePosition', [MP(2,1:2), 1490 873]);

addpath('../commonPicSource');

% Load image data 'penguin'
load('penguinViews.mat');
    
fig=figure('Color', 'white');
xLabel_list	= {'y', 'x', 'y'}; 
yLabel_list	= {'x', 'z', 'z'};
for i=1:3
    ax(i)=subplot(2,2,i+1);
    axis equal tight;
    box(ax(i),'on');
    xlabel(xLabel_list{i});
    ylabel(yLabel_list{i});
    colormap(penguin.colormap);
    hold on;
end

% Plot figures
imagesc(ax(1), penguin.y, penguin.x, penguin.img_top);
imagesc(ax(2), penguin.x, penguin.z, penguin.img_side);
imagesc(ax(3), penguin.y, penguin.z, penguin.img_front);
set(ax(1),'YDir','reverse');

% set figure size
set(fig,'Position', fig.Position-[0 0 9e2 1e1])

% put subcaptions
title_list={'a','b','c'};
for i=1:3
    ti(i)	= title(ax(i),['(',title_list{i},')']);
    set(ti(i),'Units', 'normalized');
    set(ti(i),'VerticalAlignment','top');
    set(ti(i),'FontWeight','normal');
    % move 'title' to the bottom of the plot:
    set(ti(i),'Position',ti(i).Position-[0 1 0])
    % move all 'titles' the same distance from the bottom of the plot:
    set(ti(i),'Position',ti(i).Position-[0 3e-1 0]*ax(1).PlotBoxAspectRatio(2)/ax(i).PlotBoxAspectRatio(2));
end

% Draw auxiliary axes above the figure (for png output)
for i=1:3
    axA(i)=axes(fig,'Color','none');
    box(axA(i),'on');
    set(axA(i),'xtick',[]);
    set(axA(i),'xticklabel',[]);
    set(axA(i),'ytick',[]);
    set(axA(i),'yticklabel',[]);
    set(axA(i),'Position',ax(i).Position);
    set(axA(i),'PlotBoxAspectRatio',ax(i).PlotBoxAspectRatio);
end

% Export PNG
fname	= 'trickySubfig_singlePNG';
saveas(fig,[fname,'.png'])



