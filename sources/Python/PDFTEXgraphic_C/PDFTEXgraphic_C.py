#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import scipy.io as spy
import numpy as np
from matplotlib import pyplot as plt
import matplotlib
import subprocess

# Close all figure windows
plt.close('all')                                    

# Define path to mat-files (the default path for spy.loadmat cannot be edited)
commonPicSource    = r"../../Matlab/commonPicSource/";
 
# Set screen DPI
my_dpi = 92

# matplotlib settings
matplotlib.rcParams['text.usetex']=False            # do not use LaTeX to render text (LaTeX commands are exported as they are to the SVG)
matplotlib.rcParams['svg.fonttype']='none'          # recommended way to render the text in the SVG as glyphs (assume fonts are installed on the machine where the SVG will be viewed)
matplotlib.rcParams['axes.unicode_minus']=False     # forces matplotlib to use the ASCII hyphen for the minus sign (the default Unicode is not accepted by LaTeX)
plt.rcParams["figure.autolayout"] = True            # resizes axes to fit the figure (avoids part of the figure being cut off)

# Load image data 'penguin'
# scipy supports v4 (Level 1.0), v6 and v7 to 7.2 matfiles  save in matleb as "save(...,'-v7')" )
tmp         = spy.loadmat(commonPicSource + 'penguinOutline.mat',squeeze_me=True,struct_as_record=False)
penguin     = tmp['penguin']

# Figure and figure settings
figwidth_inPx     = 231
figheight_inPx    = 424
fig=plt.figure(figsize=(figwidth_inPx/my_dpi, figheight_inPx/my_dpi), dpi=my_dpi)
ax = fig.subplots()
ax.set_aspect('equal', adjustable='box')        # set aspect ratio of axes equal
ax.autoscale(enable=True, axis='x', tight=True) # set limit of x-axis tight
ax.autoscale(enable=True, axis='y', tight=True) # set limit of y-axis tight
plt.xlabel(r'\footnotesize{\$x\$}')
plt.ylabel(r'\footnotesize{\$y\$}')

# Plot figure
ax.plot(penguin.x, penguin.y, ls='solid', linewidth=2, color='black')

# Produce SVG
fname=r"PDFTEXgraphic_C";
plt.savefig(fname + '.svg')

# Produce a PDF and a PDF_TEX file via inkscape export
subprocess.call(r"inkscape -D -z --file=" + fname + r".svg --export-pdf=" + fname + r".pdf --export-latex", shell=True)

# Set the default font size in figure to \scriptsize
subprocess.call(r"sed -i -e s'#\\begingroup#\\begingroup\\scriptsize#g' " + fname + ".pdf_tex", shell=True)

# Display the figure (put this AFTER savefig, otherwise the produced SVG is empty)
plt.show()




